﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldEdit_RimCity.WorldObjects.Cities
{
    public class WorldEditWorldObject_RimCity_City_Ghost : WorldEditWorldObject_RimCity_City
    {
        public override WorldObjectDef WorldObjectEditorDef => WorldEdit_RimCity.WorldObjects.WorldObjectDefOf.City_Ghost;
    }
}
