﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldEdit_RimCity.WorldObjects
{
    [DefOf]
    public static class WorldObjectDefOf
    {
        public static WorldObjectDef City_Faction;
        public static WorldObjectDef City_Abandoned;
        public static WorldObjectDef City_Ghost;
        public static WorldObjectDef City_Citadel;
        public static WorldObjectDef City_Compromised;
    }
}
