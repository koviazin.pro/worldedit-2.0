﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace WorldEdit_2_0.Defs
{
    [DefOf]
    public static class WorldGenStepDefOf
    {
        public static WorldGenStepDef Factions;

        public static WorldGenStepDef Features;
    }
}
