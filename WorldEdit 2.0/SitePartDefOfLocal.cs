﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldEdit_2_0
{
    [DefOf]
    public static class SitePartDefOfLocal
    {
        public static SitePartDef Turrets;

        public static SitePartDef Manhunters;

        public static SitePartDef Outpost;
    }
}
